package cz.cvut.fit.tjv.durisma1.semestral_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemestralBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemestralBeApplication.class, args);
	}

}
